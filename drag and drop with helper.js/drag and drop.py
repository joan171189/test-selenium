from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
import os

# TEST W CHROME

# Utworzenie nowej sesji Chrome :
driver = webdriver.Chrome("../chromedriver.exe")
driver.implicitly_wait(30)
driver.maximize_window()
driver.set_page_load_timeout(30)
driver.get("https://the-internet.herokuapp.com/drag_and_drop")
# driver.get("https://www.flipkart.com/search?q=laptop")
driver.maximize_window()
driver.implicitly_wait(20)

# Drag and drop - przesuwanie paska
# leftSlider = driver.find_element_by_class_name("_3aQU3C")
# rightSlider = driver.find_element_by_class_name("_3aQU3C")
# action_chains = ActionChains(driver)
# action_chains.drag_and_drop_by_offset(leftSlider, 30, 0).perform()
# time.sleep(5)
# action_chains.drag_and_drop_by_offset(rightSlider, -20, 0).perform()
# time.sleep(5)
# source = driver.find_element_by_xpath("//img[@alt='Flipkart']")
# target = driver.find_element_by_class_name("LM6RPg")
# action_chains.drag_and_drop(source, target).perform()
# time.sleep(5)
# driver.quit()
driver.save_screenshot("screenshots/screenshot.png")
# Drag and drop - metoda ActionChains, nie dająca rezultatów
source = driver.find_element_by_xpath("//*[@id='column-a']")
target = driver.find_element_by_xpath("//*[@id='column-b']")
action_chains = ActionChains(driver)
action_chains.drag_and_drop(source, target).perform()

# Drag and drop in HTML5 :
with open(os.path.abspath('drag_and_drop_helper.js'), 'r') as js_file:
    line = js_file.readline()
    script = ''
    while line:
        script += line
        line = js_file.readline()

driver.execute_script(script + "$('#column-a').simulateDragDrop({ dropTarget: '#column-b'});")
driver.save_screenshot("screenshots/screenshot1.png")
sleep(2)


# driver.quit()

