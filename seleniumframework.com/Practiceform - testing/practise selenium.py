from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import os

# TEST W CHROME

# Utworzenie nowej sesji Chrome :
driver = webdriver.Chrome("../../chromedriver.exe")
driver.implicitly_wait(30)
driver.maximize_window()
driver.get("http://www.seleniumframework.com/Practiceform/")
driver.maximize_window()
driver.implicitly_wait(20)

# TEST W FIREFOX :
# driver = webdriver.Firefox(executable_path='C:\\Users\Asia\\node_modules\\geckodriver\\geckodriver.exe')

# CONTACT US
# TC 1 - 1.1
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('Iwona Leś-Podgrzybek');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('kraj_duzych@gmail.com');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('510885920');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('Poland ');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('Kraj duzych Sp.z. o. o. ');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('proszę o kontakt');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
driver.save_screenshot("screenshots/TC 1 - 1.1.1.png")
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.1.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.2
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('424234');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('kraj_duzych.gmail.com');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('etgdrgd');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('453454');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('54543');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('43243243');
driver.save_screenshot("screenshots/TC 1 - 1.2.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.2.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.3
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('4242345435434546436454353545435453546');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('4242345435434546436454353545435453546@gmail.com');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('4242345435434546436454353545435453546');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('4242345435434546436454353545435453546');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('4242345435434546436454353545435453546');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('4242345435434546436454353545435453546');
driver.save_screenshot("screenshots/TC 1 - 1.3.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.3.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.4
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('424234543543454643645435354543545354');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('424234543543454643645435354543545354@gmail.com');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('424234543543454643645435354543545354');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('424234543543454643645435354543545354');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('424234543543454643645435354543545354');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('424234543543454643645435354543545354');
driver.save_screenshot("screenshots/TC 1 - 1.4.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.4.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.5
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('$%&^&*@&*&*@&^');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('$%^@^&%@&^@HIHIJ.&^');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('$&%^*%^%^&%^&%^&%^&*%^&');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('@#$%^&*()_)(*&^%$%^&*()');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('@#$%^&*()_(*&^%$#%^&*()(*&&^');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('@#$%^&*(*&^#$%^&*(');
driver.save_screenshot("screenshots/TC 1 - 1.5.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.5.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.6
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('tgrgdsgdgdgdg');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('fjhgdgfgdg@gfhfg.ds');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('435367457546454354');
driver.save_screenshot("screenshots/TC 1 - 1.6.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.6.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.7
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('hfghfghfhgfffffffffff');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('fhfhfhfffffffffffffffff');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('hgfhhhhhhhhhhhhhhhhf');
driver.save_screenshot("screenshots/TC 1 - 1.7.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.7.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.8
driver.save_screenshot("screenshots/TC 1 - 1.8.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[1]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.8.2.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();

# CONTACT US
# TC 1 - 1.9
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[1]/input').send_keys('Iwona Leś-Podgrzybek');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[2]/input').send_keys('kraj_duzych@gmail.com');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[3]/input').send_keys('510885920');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[4]/input').send_keys('Poland ');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/div/span[5]/input').send_keys('Kraj duzych Sp.z. o. o.');
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/span/textarea').send_keys('proszę o kontakt');
driver.save_screenshot("screenshots/TC 1 - 1.9.1.png")
driver.find_element_by_xpath('//*[@id="presscore-contact-form-widget-3"]/form/p/a[2]').click();
time.sleep(2)
driver.save_screenshot("screenshots/TC 1 - 1.9.2.png")

# SUBSCRIBE
# TC 2 - 2.1
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/p[2]/input').send_keys('kraj_duzych@gmail.com');
# time.sleep(3)
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/input[3]').click();
driver.save_screenshot("screenshots/TC 2 - 2.1.1.png")
window_before = driver.window_handles[0]
window_after = driver.window_handles[1]
driver.switch_to_window(window_after)
time.sleep(3)
# driver.find_element_by_xpath('///*[@id="pageHolder"]/div[1]/form/input[1]').send_keys('kraj_duzych@gmail.com');
# driver.find_element_by_xpath('//*[@id="recaptcha-anchor"]/div[5]').click();
driver.find_element_by_xpath('//*[@id="pageHolder"]/div[1]/form/p[2]/input').click();
driver.save_screenshot("screenshots/TC 2 - 2.1.2.png")



# SUBSCRIBE
# TC 2 - 2.2
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/p[2]/input').send_keys('kraj_duzychgmail.com');
time.sleep(3)
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/input[3]').click();
driver.save_screenshot("screenshots/TC 2 - 2.2.1.png")
window_before = driver.window_handles[0]
window_after = driver.window_handles[1]
driver.switch_to_window(window_after)
driver.find_element_by_xpath('//*[@id="pageHolder"]/div[1]/form/p[2]/input').click();
driver.save_screenshot("screenshots/TC 2 - 2.2.2.png")

# SUBSCRIBE
# TC 2 - 2.3
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/input[3]').click();
driver.save_screenshot("screenshots/TC 2 - 2.3.1.png")
window_before = driver.window_handles[0]
window_after = driver.window_handles[1]
driver.switch_to_window(window_after)
driver.find_element_by_xpath('//*[@id="pageHolder"]/div[1]/form/p[2]/input').click();
driver.save_screenshot("screenshots/TC 2 - 2.3.2.png")

# SUBSCRIBE
# TC 2 - 2.4
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/p[2]/input').send_keys('kraj_duzych@gmail.com');
time.sleep(3)
driver.find_element_by_xpath('//*[@id="text-11"]/div/form/input[3]').click();
driver.save_screenshot("screenshots/TC 2 - 2.4.1.png")
window_before = driver.window_handles[0]
window_after = driver.window_handles[1]
driver.switch_to_window(window_after)
driver.find_element_by_xpath('//*[@id="pageHolder"]/div[1]/form/p[2]/input').click();
driver.save_screenshot("screenshots/TC 2 - 2.4.2.png")

# 2) JAVA SCRIPT ALERT
#Click on the "Alert" button to generate the Simple Alert
button = driver.find_element_by_id('alert')
button.click()
#Switch the control to the Alert window
alert_obj = driver.switch_to.alert
#use the accept() method to accept the alert
alert_obj.accept()

# DRAG ME !
source = driver.find_element_by_id('draga')
# source = driver.find_element_by_xpath('//*[@id="draga"]')
target = driver.find_element_by_id('dragb')
# target = driver.find_element_by_xpath('//*[@id="dragb"]')
action_chains = ActionChains(driver)
# ssss = action_chains.click_and_hold(source).perform()
action_chains.drag_and_drop(source, target).perform()

# Switch New Browser Tab
driver.find_element_by_xpath('//*[@id="content"]/div[2]/div[2]/div/div[1]/div/p[4]/button').click()
driver.set_page_load_timeout(40)
driver.get("http://www.seleniumframework.com/Practiceform/")
driver.switch_to.window(driver.window_handles[-1])
time.sleep(5)
driver.switch_to.window(driver.window_handles[0])



driver.quit()
