from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from PIL import Image
import os
# from io import BytesIO

# TEST W CHROME

# Utworzenie nowej sesji Chrome :
driver = webdriver.Chrome("../../chromedriver.exe")
driver.implicitly_wait(30)
driver.maximize_window()
driver.set_page_load_timeout(30)
driver.get("https://letskodeit.teachable.com/p/practice")
driver.maximize_window()
driver.implicitly_wait(20)

# Radio Button Example
driver.save_screenshot("Screenshots/01.png")
driver.find_element_by_id("bmwradio").click()

selectmultiple = Select(driver.find_element_by_name('multiple-select-example'))
for index in range(len(selectmultiple.options)):
    selectmultiple.select_by_index(index)
driver.save_screenshot("Screenshots/02.png")

# Checkbox Example
driver.find_element_by_xpath("//input[@id='bmwcheck']").click()

# Switch Window Example
driver.find_element_by_xpath("//button[@id='openwindow']").click()
driver.set_page_load_timeout(40)
driver.get("https://letskodeit.teachable.com/p/practice")
driver.save_screenshot("Screenshots/03.png")

# Switch Tab Example
driver.find_element_by_xpath("//a[@id='opentab']").click()
driver.set_page_load_timeout(40)
driver.get("https://letskodeit.teachable.com/p/practice")
driver.switch_to.window(driver.window_handles[-1])
driver.close()
driver.switch_to.window(driver.window_handles[0])
driver.save_screenshot("Screenshots/04.png")

# Switch To Alert Example
driver.find_element_by_css_selector('input.inputs').send_keys("test")
driver.set_page_load_timeout(60)
driver.save_screenshot("Screenshots/05.png")
driver.get("https://letskodeit.teachable.com/p/practice")

# Mouse Hover Example
element_to_hover_over = driver.find_element_by_id("mousehover")
hover = ActionChains(driver).move_to_element(element_to_hover_over)
hover.perform()
driver.save_screenshot("Screenshots/06.png")
driver.set_page_load_timeout(60)

link = driver.find_element_by_link_text('Top')
link.click()
driver.save_screenshot("Screenshots/07.png")

# Screenshot choosen element
element = driver.find_element_by_xpath("//table[@id='product']");
location = element.location_once_scrolled_into_view;
size = element.size;
# png = driver.get_screenshot_as_png()
# driver.quit()
driver.save_screenshot("Screenshots/08.png")
driver.quit()
im = Image.open("Screenshots/08.png")
# im = Image.open(BytesIO(png))
x = location['x'];
y = location['y'];
width = location['x']+size['width'];
height = location['y']+size['height'];
im = im.crop((int(x), int(y), int(width), int(height)))
im.save('Screenshots/ima.png')



# Find first level menu object in page and move cursor on this object using method ‘move_to_element()’.
# Method perform() is used to execute the actions that we have built on action object. Do the same for all objects.
firstLevelMenu = driver.find_element_by_id("mousehover")
secondtLevelMenu = driver.find_element_by_class_name("mouse-hover-content")
action = ActionChains(driver);
kkk = action.move_to_element(firstLevelMenu)
action.click(secondtLevelMenu)
action.perform();

print (secondtLevelMenu)

for index in range(len(kkk)):
    kkk.select_by_index(index)

driver.set_page_load_timeout(60)
driver.quit()
